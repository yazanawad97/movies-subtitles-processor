package com.example.moviesubtitlesprocessor.controller;

import com.example.moviesubtitlesprocessor.service.SubtitlesProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/parse")
@CrossOrigin(origins = "*")
public class SubtitlesParserController {

    @Autowired
    SubtitlesProcessor subtitlesProcessor;

    @GetMapping(path = "/srtFiles")
    public @ResponseBody
    String getAllCandidates() {
        return subtitlesProcessor.parseFiles();
    }


}
