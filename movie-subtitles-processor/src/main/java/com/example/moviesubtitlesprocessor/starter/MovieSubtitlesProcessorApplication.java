package com.example.moviesubtitlesprocessor.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.example.moviesubtitlesprocessor")
public class MovieSubtitlesProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieSubtitlesProcessorApplication.class, args);
	}

}
