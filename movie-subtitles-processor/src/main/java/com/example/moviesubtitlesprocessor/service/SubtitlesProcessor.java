package com.example.moviesubtitlesprocessor.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SubtitlesProcessor {

    @Value("${mkv.folder.path}")
    String mkvFolderPathString;

    @Value("${srt.folder.path}")
    String srtFolderPathString;

    @Value("${season}")
    String season;

    @Value("${episode.digit.type}")
    String digitType;

    @Value("${episode.symbol}")
    String episodeSymbol;

    public String parseFiles() {
        List<File> mkvFilesList = new ArrayList<>();
        List<File> srtFilesList = new ArrayList<>();
        File[] mkvFiles = new File(mkvFolderPathString).listFiles();
        File[] srtFiles = new File(srtFolderPathString).listFiles();
        if (Objects.nonNull(mkvFiles) && Objects.nonNull(srtFiles)) {

            for (File file : mkvFiles) {
                if (file.isFile()) {
                    if (file.getName().endsWith(".mkv"))
                        mkvFilesList.add(file);
                }
            }
            for (File file : srtFiles) {
                if (file.isFile()) {
                    if (file.getName().endsWith(".srt"))
                        srtFilesList.add(file);
                }
            }
            AtomicInteger counter = new AtomicInteger(1);
            mkvFilesList.forEach(mkvFile -> {
                File srt = srtFilesList.stream()
                        .filter(srtFile -> srtFile.getName().contains(season) && srtFile.getName().contains(episodeSymbol + resolveCounter(counter)))
                        .findFirst().get();
                try {
                    Files.move(Paths.get(srt.getPath()), Paths.get(mkvFile.getPath().substring(0, mkvFile.getPath().length() - 4) + ".srt"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                counter.getAndIncrement();
            });
            return "Success";
        } else return "Files Not Found !";
    }

    private String resolveCounter(AtomicInteger counter) {
        if (digitType.equals("2") && counter.get() < 10)
            return "0" + counter.get();
        return String.valueOf(counter.get());
    }
}
